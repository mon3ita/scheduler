from flask import Flask
from flask_cors import CORS

from .models import DB

app = Flask(__name__, static_folder="public/static", template_folder="public/views")
db = DB()

CORS(app)

app.config["SECRET_KEY"] = "8HewssFgEufGQ8np"

from .routes import day, home, schedule, user

if __name__ == "__main__":
    app.run()
