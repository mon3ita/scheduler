from sqlalchemy import (
    Table,
    Integer,
    Column,
    Integer,
    Date,
    Boolean,
    String,
    Text,
    ForeignKey,
)

from . import META

schedules = Table(
    "schedules",
    META,
    Column("id", Integer, primary_key=True),
    Column("title", String(150), nullable=False),
    Column("description", String(250), nullable=False),
    Column(
        "user_id", Integer, ForeignKey("users.id"), nullable=False, on_delete="CASCADE"
    ),
)

days = Table(
    "days",
    META,
    Column("id", Integer, primary_key=True),
    Column("title", String(150), nullable=False),
    Column("description", Text, nullable=False),
    Column("date", Date, nullable=False),
    Column("done", Boolean, unique=False, default=True),
    Column(
        "schedule_id",
        Integer,
        ForeignKey("schedules.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column(
        "user_id", Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False,
    ),
)
