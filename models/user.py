from sqlalchemy import Table, Column, String, Text, Integer

from . import META

users = Table(
    "users",
    META,
    Column("id", Integer, primary_key=True),
    Column("username", String(20), nullable=False, unique=True),
    Column("password", Text, nullable=False),
)
