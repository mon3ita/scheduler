from sqlalchemy import create_engine, MetaData, text

META = MetaData()


class DB:
    def __init__(self):
        self._eng = create_engine(
            "sqlite:///scheduler.db",
            connect_args={"check_same_thread": False},
            echo=True,
        )
        self.con = self._eng.connect()
        self.create()

    def create(self):
        from .user import users
        from .schedule import schedules, days

        self.users = users
        self.schedules = schedules
        self.days = days

        META.create_all(self._eng)

    def select(self, query):
        s = text(query)
        result = self.con.execute(s)
        return result.fetchall()

    def select_one(self, query):
        s = text(query)
        result = self.con.execute(s)
        return result.fetchone()

    def insert(self, table, **query):
        keys = ",".join(query.keys())
        values = ",".join([f"'{value}'" for value in query.values()])
        query_ = text(f"INSERT INTO {table}({keys}) VALUES({values})")
        result = self.con.execute(query_)
        return result

    def update(self, query):
        s = text(query)
        result = self.con.execute(s)
        return result

    def delete(self, query):
        s = text(query)
        result = self.con.execute(s)
        return result
