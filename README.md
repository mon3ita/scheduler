# Scheduler

```
1. Clone the repo.

2. Create virtualenv and activate it.

3. Install all dependencies, by running pip install -r requirements.txt

4. Run the project by executing flask run.

Enjoy :)
```