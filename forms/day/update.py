from flask_wtf import FlaskForm
from wtforms import StringField, TextField, DateField, BooleanField, SubmitField

from wtforms.validators import DataRequired


class UpdateForm(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    description = TextField("Description", validators=[DataRequired()])
    date = DateField("Date")
    done = BooleanField("Done")
    submit = SubmitField("Update")
