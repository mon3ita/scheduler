from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SubmitField, DateField

from wtforms.validators import DataRequired


class CreateForm(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    description = TextField("Description", validators=[DataRequired()])
    date = DateField("Date", validators=[DataRequired()])
    submit = SubmitField("Create")
