from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SubmitField

from wtforms.validators import DataRequired


class CreateForm(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    description = TextField("Description", validators=[DataRequired()])
    submit = SubmitField("Create")
