from ..app import app, db

import jwt
import bcrypt

from flask import json, jsonify, request, redirect, render_template, make_response
from .jwt import login_required

from ..forms.user import LoginForm, RegisterForm


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    error = None
    if form.validate_on_submit():
        username = form.username.data
        query = f"SELECT * FROM users WHERE username='{username}'"
        user = db.select_one(query)

        if user and bcrypt.checkpw(
            form.password.data.encode("utf-8"), user[2].encode("utf-8")
        ):
            token = jwt.encode(
                {"id": user[0], "username": user[1]}, app.config["SECRET_KEY"]
            )
            username = form.username.data
            response = make_response(
                render_template("home/index.html", username=username)
            )
            response.set_cookie("token", token)
            return response
        elif not user:
            error = "User doesn't exist."
        else:
            error = "Wrong credentials given."

    return render_template("user/login.html", form=form, error=error)


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegisterForm()
    error = None

    if form.validate_on_submit():
        if form.password.data != form.password_confirmation.data:
            return redirect("/login")
        user = {"username": form.username.data, "password": form.password.data}

        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(user["password"].encode("utf-8"), salt)
        user["password"] = hashed.decode("utf-8")

        try:
            user = db.insert(
                "users", username=user["username"], password=user["password"]
            )
            return redirect("/login")
        except Exception as e:
            print(e)
            error = {"message": "User with given username already exists."}

    return render_template("user/register.html", form=form, error=error)


@app.route("/logout", methods=["GET", "DELETE"])
@login_required
def logout(user):
    response = make_response(redirect("/login"))
    response.set_cookie("token", "", max_age=0)
    return response
