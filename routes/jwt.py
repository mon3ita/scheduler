from functools import wraps

from flask_jwt import JWT, jwt_required, current_identity
from flask import request, render_template, redirect

import jwt

from ..app import app, db


def login_required(func):
    @wraps(func)
    def decorator(*args, **kwargs):

        token = request.cookies.get("token", None)
        if token:
            try:
                data = jwt.decode(token, app.config["SECRET_KEY"])
                username = data["username"]
                query = f"SELECT * FROM users WHERE username='{username}'"
                user = db.select_one(query)
            except Exception as e:
                return redirect("/login")

            return func(user, *args, **kwargs)
        else:
            return redirect("/login")

    return decorator
