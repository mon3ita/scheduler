from flask import render_template, request, redirect

from ..app import app, db

from .jwt import login_required

from ..forms.schedule import CreateForm, UpdateForm


@app.route("/schedules", methods=["GET"])
@login_required
def schedules(user):
    user_id = user["id"]
    query = f"SELECT * FROM schedules WHERE user_id={user_id}"
    schedules = db.select(query)
    return render_template(
        "schedule/index.html", schedules=schedules, username=user["username"]
    )


@app.route("/schedules/<schedule_id>", methods=["GET"])
@login_required
def schedule(user, schedule_id):
    schedule_ = db.select_one(
        f"SELECT * FROM schedules WHERE user_id={user.id} AND id={schedule_id}"
    )
    days = db.select(f"SELECT * FROM days WHERE schedule_id={schedule_id}")
    return render_template(
        "schedule/schedule.html",
        schedule=schedule_,
        username=user["username"],
        days=days,
    )


@app.route("/schedules/create", methods=["GET", "POST"])
@login_required
def create(user):
    form = CreateForm()

    errors = []
    if request.method == "POST":
        if form.validate_on_submit():
            db.insert(
                "schedules",
                title=form.title.data,
                description=form.description.data,
                user_id=user.id,
            )
            return redirect(f"/schedules")
        errors = form.errors
    return render_template(
        "schedule/create.html", form=form, username=user["username"], errors=errors
    )


@app.route("/schedules/<schedule_id>/update", methods=["GET", "POST"])
@login_required
def update(user, schedule_id):
    schedule_ = db.select_one(
        f"SELECT * FROM schedules WHERE user_id={user.id} AND id={schedule_id}"
    )

    if not schedule_:
        return redirect("/schedules")

    form = UpdateForm(title=schedule_.title, description=schedule_.description)

    errors = []
    if request.method == "POST":
        if form.validate_on_submit():
            result = db.update(
                f"UPDATE schedules SET title='{form.title.data}', description='{form.description.data}' WHERE id={schedule_id}"
            )
            return redirect(f"/schedules/{schedule_id}")
        errors = form.errors
    return render_template(
        "schedule/update.html", form=form, username=user["username"], errors=errors
    )


@app.route("/schedules/<schedule_id>/delete", methods=["GET", "DELETE"])
@login_required
def delete(user, schedule_id):
    schedule_ = db.select_one(
        f"SELECT * FROM schedules WHERE user_id={user.id} AND id={schedule_id}"
    )
    if not schedule_:
        return redirect("/schedules")

    db.delete(f"DELETE FROM schedules WHERE id={schedule_id}")
    return redirect("/schedules")
