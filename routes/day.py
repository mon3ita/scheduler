from flask import render_template, request, redirect

from datetime import datetime

from ..app import app, db
from .jwt import login_required

from ..forms.day import CreateForm, UpdateForm


@app.route("/schedules/<schedule_id>/create", methods=["GET", "POST"])
@login_required
def add(user, schedule_id):
    form = CreateForm()

    errors = []
    if request.method == "POST":
        form.date.data = datetime.strptime(form.date.raw_data[0], "%Y-%m-%d")
        if form.validate_on_submit():
            day = db.insert(
                "days",
                title=form.title.data,
                description=form.description.data,
                date=form.date.data,
                schedule_id=schedule_id,
                user_id=user.id,
            )
            return redirect(f"/schedules/{schedule_id}")

        errors = form.errors

    return render_template(
        "day/create.html", form=form, username=user["username"], errors=errors
    )


@app.route("/schedules/<schedule_id>/<day_id>", methods=["GET"])
@login_required
def get(user, schedule_id, day_id):
    day = db.select(
        f"SELECT * FROM days WHERE id={day_id} AND schedule_id={schedule_id} AND user_id={user.id}"
    )[0]

    if not day:
        return redirect("/schedules")

    return render_template("day/day.html", day=day, username=user["username"])


@app.route("/schedules/<schedule_id>/<day_id>/update", methods=["GET", "POST"])
@login_required
def update_day(user, schedule_id, day_id):
    day = db.select(
        f"SELECT * FROM days WHERE id={day_id} AND schedule_id={schedule_id} AND user_id={user.id}"
    )[0]
    if not day:
        return redirect(f"/schedules/")

    form = UpdateForm(
        title=day.title,
        description=day.description,
        date=datetime.strptime(day.date[: day.date.index(" 00")], "%Y-%m-%d"),
    )
    errors = []
    if request.method == "POST":
        form.done.data = (
            1 if len(form.done.raw_data) > 1 and form.done.raw_data[1] == "on" else 0
        )
        if form.validate_on_submit():
            updated = db.update(
                f"UPDATE days SET title='{form.title.data}', description='{form.description.data}', date='{form.date.data}', done={form.done.data} WHERE id={day.id}"
            )
            return redirect(f"/schedules/{schedule_id}/{day_id}")
        errors = form.errors

    print(day.done)
    done = "checked=checked" if day.done else ""
    return render_template(
        "day/update.html", day=day, form=form, errors=errors, done=done
    )


@app.route("/schedules/<schedule_id>/<day_id>/delete", methods=["GET", "DELETE"])
@login_required
def delete_day(user, schedule_id, day_id):
    day = db.select(
        f"SELECT * FROM days WHERE id={day_id} AND schedule_id={schedule_id} AND user_id={user.id}"
    )[0]
    if not day:
        return redirect("/schedules")

    result = db.delete(f"DELETE FROM days WHERE id={day_id}")
    return redirect(f"/schedules/{schedule_id}")
